echo ---change to branch : $1
cd /Users/surgimap/workspace/surgimap/surgimap-2/Surgimap
rm -rf "Surgimap for Mac.app"
echo $dynamic
git reset --hard
git pull
git checkout $1
git pull

echo ----mac os x compiler---- 
if [ $dynamic = "yes" ]
then
echo "compile dynamic for the frog"
/Users/surgimap/Qt/5.5/clang_64/bin/qmake surgimap.pro "CONFIG+=surgimap_mac_dynamic" -r -spec macx-clang
else
echo "compile static"
/usr/local/Qt-5.5.1/bin/qmake surgimap.pro -r -spec macx-clang
fi


echo ‘change make file to include cache’
perl -pi -e 's/CC            = \/Applications\/Xcode.app\/Contents\/Developer\/Toolchains\/XcodeDefault.xctoolchain\/usr\/bin\/clang/CC = ccache clang/g' Makefile

perl -pi -e 's/CXX           = \/Applications\/Xcode.app\/Contents\/Developer\/Toolchains\/XcodeDefault.xctoolchain\/usr\/bin\/clang++/CXX = ccache clang/g' Makefile

perl -pi -e 's/-lqmltestplugin/ /g' Makefile
perl -pi -e 's/-lQt5QuickTest/ /g' Makefile
perl -pi -e 's/-lQt5Test/ /g' Makefile

echo "take pluging of"


#perl -pi -e 's/Q_IMPORT_PLUGIN+\(QTestQmlModule+\)/ /g' surgimap\ for\ mac_qml_plugin_import.cpp
#perl -pi -e 's/Q_IMPORT_PLUGIN+\(QTestQmlModule+\)/ /g' surgimap\ for\ mac_qml_plugin_import.cpp
#perl -pi -e 's/Q_IMPORT_PLUGIN+\(QtQuickExtrasPlugin+\)/ /g' surgimap\ for\ mac_qml_plugin_import.cpp
#perl -pi -e 's/Q_IMPORT_PLUGIN+\(QtQmlModelsPlugin+\)/ /g' surgimap\ for\ mac_qml_plugin_import.cpp

echo -e \#include\ \<QtPlugin\>\\nQ_IMPORT_PLUGIN\(QtQuick2Plugin\)\\nQ_IMPORT_PLUGIN\(QMultimediaDeclarativeModule\)\\nQ_IMPORT_PLUGIN\(QtQuick2WindowPlugin\)\\nQ_IMPORT_PLUGIN\(QtQuickControlsPlugin\)\\nQ_IMPORT_PLUGIN\(QtQuickLayoutsPlugin\)\\nQ_IMPORT_PLUGIN\(QtQuick2DialogsPlugin\)\\nQ_IMPORT_PLUGIN\(QmlFolderListModelPlugin\)\\nQ_IMPORT_PLUGIN\(QmlSettingsPlugin\)\\nQ_IMPORT_PLUGIN\(QtQuick2DialogsPrivatePlugin\)\\nQ_IMPORT_PLUGIN\(QtQuick2PrivateWidgetsPlugin\)\\n > surgimap\ for\ mac_qml_plugin_import.cpp




#-lqmltestplugin -lQt5QuickTest -lQt5Test


#export CC="ccache clang"
#export CXX="ccache clang++"
#export CCACHE_CPP2=yes

make clean
make -j4 > report.txt 
retcode=$?

echo ———parsing— for hash code and email—$1
cd /Users/surgimap/workspace/surgimap/surgimap-2/Surgimap
git rev-parse --short $1>outtxt
hashcode=$(head -n 1 outtxt)
git show $hashcode --pretty=format:%ce>outtxt
GIT_AUTHOR_EMAIL=$(head -n 1 outtxt)
echo $hashcode $GIT_AUTHOR_EMAIL
echo —————


if [ $retcode != 0 ]
then

echo problem compiling see the .log
exit 0

else

echo --compile succed
echo ---embeding libraries and copy to network

echo --copy SurgimapDatabase folder into .app pkg
cp -R /Users/surgimap/workspace/surgimap/SurgimapDatabase/ "/Users/surgimap/workspace/surgimap/surgimap-2/Surgimap/Surgimap for Mac.app/Contents/SurgimapDatabase/"

bash /Users/surgimap/jenkinsscript/sign.sh
if [ $dynamic = "yes" ]
then
echo "network copy for the frog"
bash /Users/surgimap/jenkinsscript/dynamicnetworkcopy.sh $1 $hashcode
else
bash /Users/surgimap/jenkinsscript/networkcopy.sh $1 $hashcode
fi

bash /Users/surgimap/jenkinsscript/reportmail.sh "succ" $GIT_AUTHOR_EMAIL $1

fi


echo ---finish---
