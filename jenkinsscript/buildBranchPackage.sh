#!/bin/bash

echo "this is the first step to generate an installer"
echo "Building Master Package for $1"

buildsuccess=1

cd /Users/surgimap/jenkinsscript


rm /Users/surgimap/jenkinsscript/binaryToNotarize/branch.txt
echo $1 >> /Users/surgimap/jenkinsscript/binaryToNotarize/branch.txt

echo "Building branch $1"
bash buildbranch.sh $1

if [[ $? == 0 ]]; then
    echo "Build master success. Triggering bundle and installer builds."

else
    echo "Build master failed. Stopping here.";
    exit -1
fi

if [ $1 = "master" ]
then
surgimapAppPath=/Users/surgimap/workspace/surgimap/surgimap-2
else
surgimapAppPath=/Users/surgimap/developement/sm_dev
fi

rm -r "/Users/surgimap/jenkinsscript/binaryToNotarize/Surgimap for Mac.app/"
mkdir -p "/Users/surgimap/jenkinsscript/binaryToNotarize/Surgimap for Mac.app/"
cp -R "$surgimapAppPath/Surgimap/Surgimap for Mac.app" "/Users/surgimap/jenkinsscript/binaryToNotarize/"

bash notarize.sh "Surgimap for Mac.app" $surgimapAppPath/Surgimap $1 sm

echo "so far we did build surgimap for mac and did submit for approvel to apple services"
echo "save some info to run the next step which isnotarizeFinishedTriggerBuild.sh which is a job in jenkins "

echo "Recording current branch for vendor builds"
echo -n $1 > /Users/surgimap/vendorbuilds/current_branch.txt

echo "Getting latest commit for $1"

cd $surgimapAppPath
pwd

commit=$(git rev-parse --short $1)

for field in $(grep "VERSION =" Surgimap/surgimap.pro); do
version=$field;
done

echo version=$version > /Users/surgimap/vendorbuilds/current_version.txt
echo $version > /Users/surgimap/vendorbuilds/current_version_raw.txt
echo -n $commit > /Users/surgimap/vendorbuilds/current_commit.txt

echo "create the file do next step"
echo -n "notarizeFinishedTriggerBuild.sh will consume this file" > /Users/surgimap/vendorbuilds/nextStep.txt

echo "all good" $buildsuccess
exit 0


