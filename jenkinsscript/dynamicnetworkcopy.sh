cd /Users/surgimap/workspace/surgimap/surgimap-2/Surgimap
#buildserver SurgimapBS
echo "copy this build for the frog"

# these should be done before signing, moving to buildbranch.sh
# /Users/surgimap/Qt/5.6/clang_64/bin/macdeployqt "Surgimap for Mac.app"
# bash /Users/surgimap/jenkinsscript/dynamicBuildQmlCopy.sh

echo "mounting network drive and making directory if it doesnt exist"
#mount -t smbfs //automation:1E%23IP%2Aautog1@10.0.10.4/surgimap /MacBuild/networkshare
mkdir -p "/MacBuild/networkshare/Builds/Mac/squish"

echo ---copy to network
rm -rf "/MacBuild/networkshare/Builds/Mac/squish/Surgimap for Mac.app"
cp -R "Surgimap for Mac.app" "/MacBuild/networkshare/Builds/Mac/squish/Surgimap for Mac.app"
cp -R "Surgimap for Mac.app" "/MacBuild/networkshare/Builds/Mac/squish/Surgimap for Mac-$2.app"
cd /Users/surgimap/workspace/surgimap/surgimap-2/Surgimap

echo ---cleaning up
#umount /MacBuild/networkshare

echo "copyed to"
echo "/MacBuild/networkshare/Builds/Mac/$1/$2/Surgimap for Mac.app"
