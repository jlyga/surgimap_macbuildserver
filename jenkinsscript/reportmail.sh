cd /Users/surgimap/jenkinsscript
if [ $1 = "succ" ] 
then
    ./mailsend -to $2 -from surgimap.auto.compile@gmail.com -starttls -port 587 -auth -smtp smtp.gmail.com -M "Success! A Mac build of the branch "$3" has been successfully compiled. Binaries have been copied into the shared drive into the Builds\\mac\\"$3"\\"$4 -sub "Surgimap Compilation Result" -user surgimap.auto.compile -pass Surgimap123
else
    ./mailsend -to $2 -from surgimap.auto.compile@gmail.com -starttls -port 587 -auth -smtp smtp.gmail.com -M "Uh oh, the Mac build failed of "$3". Please see attachment for detailed information." -sub "Surgimap Compilation Result" -attach "report.txt" -user surgimap.auto.compile -pass Surgimap123
fi
echo -------email sent----------
