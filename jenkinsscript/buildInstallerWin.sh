echo "Building Windows Installer Package for $2"

# create working vars
settingsSrc="/Users/surgimap/sminstall/builds/$1/.installersettings.ini"
installSrc="/Users/surgimap/sminstall_windows/installers/$1/$2Installer.exe"
installDest="/Users/surgimap/vendorbuilds/installertmp/windows"

# clean working dir(s)
echo "Cleaning working directory"
rm -rf "$installDest/*"

# run installerBuilder to compile the assets
# installerBuilder <installer-path> <settings-path> <bundle-path> <output-path>
/Users/surgimap/sminstall_windows/installerBuilder_new/installerBuilder "$installSrc" "$settingsSrc" /Users/surgimap/vendorbuilds/shared_assets/SM2MacWindowsBundle $installDest/$2.exe 9