
cd $surgimapSrcPath
echo —use qt deployment tool in $surgimapSrcPath
/usr/local/Qt-5.12.5/bin/macdeployqt "Surgimap for Mac.app" -qmldire=$surgimapSrcPath
#no need to copy qml anymore
#cp -R /Users/surgimap/qml/ "/Users/surgimap/workspace/surgimap/surgimap-2/Surgimap/Surgimap for Mac.app/Contents/Resources"

echo --add libraries
echo —sign frameworks

#codesign --force --verify --sign 228BC59BBB2F284B1AA554F0D751085BEF3DF759 "Surgimap for Mac.app/Contents/Frameworks/libopenjpeg.5.dylib"

echo ---sign plugins

mkdir -p "Surgimap for Mac.app/Contents/smPlugins"
cp /Users/surgimap/smPlugins/libopenjpeg.1.4.0.dylib "Surgimap for Mac.app/Contents/smPlugins/libopenjpeg.1.4.0.dylib"
codesign --force --verify --sign 228BC59BBB2F284B1AA554F0D751085BEF3DF759 "Surgimap for Mac.app/Contents/smPlugins/libopenjpeg.1.4.0.dylib"

echo --sign the app
codesign --deep --force --verify --sign 228BC59BBB2F284B1AA554F0D751085BEF3DF759 "Surgimap for Mac.app"

echo --verification
codesign -vvv -d "Surgimap for Mac.app"

spctl -a -t exec -vv "Surgimap for Mac.app"

#bash /Users/surgimap/jenkinsscript/notarize.sh























