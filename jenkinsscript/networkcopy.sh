set -e

echo ----- ----- network copy begin ----- -----

#cd /Users/surgimap/workspace/surgimap/surgimap-2/Surgimap
#buildserver SurgimapBS

echo ----- ----- mount ----- -----
#mount -t smbfs //automation:1E%23IP%2Aautog1@10.0.10.4/Surgimap /MacBuild/networkshare || echo file share already mounted
#mount -t smbfs //'FS-VM;SurgimapFS':Surgimap2%40@10.2.10.110/Surgimap /MacBuild/networkshare || echo file share already mounted

#mkdir -p "/MacBuild/networkshare/Builds/Mac/$1/$2"

# extract filename
filename=$(echo -n $1 | rev | cut -d '/' -f1 | rev)
source="$1"

if [ -z "$3" ]
then
	echo ----- ----- alias not set, straight copy ----- -----
	destination="/MacBuild/networkshare/$2/$filename"
else
	echo ----- ----- alias set, copy to $3 ----- -----
	destination="/MacBuild/networkshare/$2/$3"
fi

echo ----- ----- cleanup destination ----- -----
echo ----- ----- removing $destination if it exists ----- -----
rm -rf "$destination" || echo file or dir doesnt exist yet
mkdir -p "/MacBuild/networkshare/$2" || echo dir already exists

echo ----- ----- copying $filename to network ----- -----
#rm -rf "/MacBuild/networkshare/Builds/Mac/$1/$2/Surgimap for Mac.app"
#cp -R "Surgimap for Mac.app" "/MacBuild/networkshare/Builds/Mac/$1/$2/Surgimap for Mac.app"
cp -R "$source" "$destination"

##cd /Users/surgimap/workspace/surgimap/surgimap-2/Surgimap
echo ----- ----- unmount ----- -----
#umount /MacBuild/networkshare || echo file share already unmounted
echo ----- ----- copied to ----- -----
echo "$destination"
echo ----- ----- network copy end ----- -----
#echo "/MacBuild/networkshare/Builds/Mac/$1/$2/Surgimap for Mac.app"
