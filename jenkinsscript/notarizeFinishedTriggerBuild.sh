test -f /Users/surgimap/vendorbuilds/nextStep.txt
if [[ $? == 0 ]]; then
    echo "we got pending vendore build to see if surgimap for mac is notorized by apple services"
    
else
    echo "nothing to, see you in the next 5 minute";
    exit -1
fi;


branch=$(cat /Users/surgimap/vendorbuilds/current_branch.txt)
commit=$(cat /Users/surgimap/vendorbuilds/current_commit.txt)

echo "doing $branch and $commit"

xcrun stapler staple -v /MacBuild/networkshare/Builds/Mac/$branch/$commit/Surgimap\ for\ Mac.app
if [ $? -eq 0 ]; then
echo "Successfully stepled Surgimap\ for\ Mac.app"
else
echo "error Surgimap\ for\ Mac.app"
exit -1
fi

echo "surgimap is notarized and stepled so all good, trigger vendore build now"

/Users/surgimap/jenkinsscript/buildBranchBundle.sh $branch

if [[ $? == 0 ]]; then
    echo "Core bundle built successfully. Triggering vendor builds."
    buildsuccess=0
else
    echo "Core bundle build failed. Stopping here.";
fi;

rm /Users/surgimap/vendorbuilds/nextStep.txt
echo -n "notarizeDmgFilesFinished.sh will consume this file" > /Users/surgimap/vendorbuilds/checkDmgFiles.txt

echo "all good" $buildsuccess
exit 0