#!/bin/bash

echo "Building Custom Package for Branch: $1"

buildsuccess=1

cd /Users/surgimap/jenkinsscript

echo "Building branch $1"
bash buildbranch.sh $1

if [[ $? == 0 ]]; then
    echo "Build $1 success. Triggering bundle build."
    bash buildCustomBundle.sh $1

    if [[ $? == 0 ]]; then
        echo "Core bundle built successfully."
        buildsuccess=0
    else
        echo "Core bundle build failed. Stopping here.";
    fi;
else
    echo "Build $1 failed. Stopping here.";
fi

exit $buildsuccess
