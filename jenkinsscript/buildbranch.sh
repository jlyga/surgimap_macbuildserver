mount -t smbfs //automation:1E%23IP%2Aautog1@10.0.10.4/Surgimap /MacBuild/networkshare

set -e

echo ---change to branch : $1
export surgimapCurrentBranch=$1
if [ $1 = "master" ]
then
export surgimapSrcPath=/Users/surgimap/workspace/surgimap/surgimap-2/Surgimap
else
export surgimapSrcPath=/Users/surgimap/developement/sm_dev/Surgimap
fi

cd $surgimapSrcPath
rm -rf "Surgimap for Mac.app"
rm -rf "Mac.app"
echo $dynamic
git reset --hard
git pull
git checkout -f $1
git pull


echo "as for Qt5.6.2 and up  we need to clear stuff by our self"
rm .qmake.stash || echo ".qmake.stash already removed"
rm -rf ".ui" || echo ".ui already removed"
rm -rf ".obj" || echo ".obj already removed"
rm -rf ".moc" || echo ".moc already removed"
rm "surgimap for mac_qml_plugin_import.cpp" || echo "surgimap for mac_qml_plugin_import.cpp already removed"
rm "surgimap for mac_plugin_import.cpp" || echo "surgimap for mac_plugin_import.cpp already removed"

echo "making sure dynamic parameter is not empty"
if [ -z "$dynamic" ]
then
echo "dynamic is empty, filling in no"
dynamic="no"
else
echo "dynamic is set to $dynamic" 
fi

echo ----mac os x compiler---- 
if [ $dynamic = "yes" ]
then
	echo "compile dynamic for the frog using Qt 5.6"
	/Users/surgimap/Qt/5.6/clang_64/bin/qmake surgimap.pro "CONFIG+=surgimap_has_breakpads" "CONFIG+=CONFIG+=surgimap_mac_dynamic" -r -spec macx-clang
else
	echo "compile static"
	#if [ ${1:0:6} = "master" ]
	#then
	#	echo using qt 5.6.2
	#	/usr/local/Qt-5.6.2/bin/qmake surgimap.pro "CONFIG+=surgimap_has_breakpads" "CONFIG+=surgimap_mac_static" -r -spec macx-clang
	#else
		echo doing plugin first
		if [ -f ../Plugins/Sample/gmplugin/gmplugin.pro ]
	    then
	    	echo qmake surgimap first to generate git info file Surgimap/core/util/git_information.h
	    	#/usr/local/Qt-5.12.5/bin/qmake surgimap.pro "CONFIG+=surgimap_has_breakpads" "CONFIG+=surgimap_mac_static" "CONFIG+=qtquickcompiler" -r -spec macx-clang
			#/usr/local/Qt-5.12.5/bin/qmake ../Plugins/Sample/gmplugin/gmplugin.pro
			#perl -pi -e 's/-lqmltestplugin/ /g' Makefile
			#perl -pi -e 's/-lQt5QuickTest/ /g' Makefile
			#perl -pi -e 's/-lQt5Test/ /g' Makefile
			#make clean
			#make -j2
		else
	     	echo pluging not found on this branch so skip directly to compile it
		fi

		echo using qt 5.12.5 to build surgimap
		/usr/local/Qt-5.12.5/bin/qmake surgimap.pro "CONFIG+=surgimap_has_breakpads" "CONFIG+=surgimap_mac_static" "CONFIG+=qtquickcompiler" -r -spec macx-clang

    #    echo using qt 5.11.1
    #    /Users/surgimap/qt5.11-mac-buildserver/bin/qmake surgimap.pro "CONFIG+=surgimap_has_breakpads" "CONFIG+=surgimap_mac_static" -r -spec macx-clang
	#fi
fi


echo 'change make file to include cache'
#perl -pi -e 's/CC            = \/Applications\/Xcode.app\/Contents\/Developer\/Toolchains\/XcodeDefault.xctoolchain\/usr\/bin\/clang/CC = ccache clang/g' Makefile

#perl -pi -e 's/CXX           = \/Applications\/Xcode.app\/Contents\/Developer\/Toolchains\/XcodeDefault.xctoolchain\/usr\/bin\/clang++/CXX = ccache clang/g' Makefile

perl -pi -e 's/-lqmltestplugin/ /g' Makefile
perl -pi -e 's/-lQt5QuickTest/ /g' Makefile
perl -pi -e 's/-lQt5Test/ /g' Makefile

echo "take pluging of"


#perl -pi -e 's/Q_IMPORT_PLUGIN+\(QTestQmlModule+\)/ /g' surgimap\ for\ mac_qml_plugin_import.cpp
#perl -pi -e 's/Q_IMPORT_PLUGIN+\(QTestQmlModule+\)/ /g' surgimap\ for\ mac_qml_plugin_import.cpp
#perl -pi -e 's/Q_IMPORT_PLUGIN+\(QtQuickExtrasPlugin+\)/ /g' surgimap\ for\ mac_qml_plugin_import.cpp
# to renable for 5.5.1 perl -pi -e 's/Q_IMPORT_PLUGIN+\(QtQmlModelsPlugin+\)/ /g' surgimap\ for\ mac_qml_plugin_import.cppecho -e \#include\ \<QtPlugin\>\\nQ_IMPORT_PLUGIN\(QtQuick2Plugin\)\\nQ_IMPORT_PLUGIN\(QMultimediaDeclarativeModule\)\\nQ_IMPORT_PLUGIN\(QtQuick2WindowPlugin\)\\nQ_IMPORT_PLUGIN\(QtQuickControlsPlugin\)\\nQ_IMPORT_PLUGIN\(QtQuickLayoutsPlugin\)\\nQ_IMPORT_PLUGIN\(QtQuick2DialogsPlugin\)\\nQ_IMPORT_PLUGIN\(QmlFolderListModelPlugin\)\\nQ_IMPORT_PLUGIN\(QmlSettingsPlugin\)\\nQ_IMPORT_PLUGIN\(QtQuick2DialogsPrivatePlugin\)\\nQ_IMPORT_PLUGIN\(QtQuick2PrivateWidgetsPlugin\)\\n > surgimap\ for\ mac_qml_plugin_import.cpp




#-lqmltestplugin -lQt5QuickTest -lQt5Test


#export CC="ccache clang"
#export CXX="ccache clang++"
#export CCACHE_CPP2=yes

#make clean
make -j 6 > report.txt

retcode=$?

echo ———parsing— for hash code and email—$1

cd $surgimapSrcPath

git rev-parse --short $1>outtxt
hashcode=$(head -n 1 outtxt)
git show $hashcode --pretty=format:%ce>outtxt
GIT_AUTHOR_EMAIL=$(head -n 1 outtxt)
echo $hashcode $GIT_AUTHOR_EMAIL
echo —————

if [ $retcode != 0 ]
then

echo retcode $retcode
echo problem compiling see the .log
exit 1

else

echo --compile succed
echo ---embeding libraries and copy to network
echo disable signing
bash /Users/surgimap/jenkinsscript/sign.sh

retcode=$?

if [ $retcode != 0 ]
then
echo issue signing executable
exit 1
fi

if [ $dynamic = "yes" ]
then
echo "network copy for the frog"
#bash /Users/surgimap/jenkinsscript/dynamicnetworkcopy.sh $1 $hashcode
bash /Users/surgimap/jenkinsscript/networkcopy.sh "/Users/surgimap/workspace/surgimap/surgimap-2/Surgimap/Surgimap for Mac.app" "Builds/Mac/squish"
bash /Users/surgimap/jenkinsscript/networkcopy.sh "/Users/surgimap/workspace/surgimap/surgimap-2/Surgimap/Surgimap for Mac.app" "Builds/Mac/squish" "Surgimap for Mac-$hashcode.app"
else
bash /Users/surgimap/jenkinsscript/networkcopy.sh "$surgimapSrcPath/Surgimap for Mac.app" "Builds/Mac/$1/$hashcode"
fi

bash /Users/surgimap/jenkinsscript/reportmail.sh "succ" $GIT_AUTHOR_EMAIL $1 $hashcode

fi


echo ---finish---
