echo "Changing to installer code repo"
cd /Users/surgimap/sminstall/

echo "Clearing build directory"
rm -rf ./builds/$1
mkdir -p ./builds/$1

echo "Populating vendor-specific assets"
# change TARGET in sminstall.pro
python replace.py SmInstall.pro "$2"

# copy dynamic resources
rm -rf ./resources/images
cp -R /Users/surgimap/vendorbuilds/$1/resources/images ./resources/images

# copy icon
rm surgimap.icns
cp -R /Users/surgimap/vendorbuilds/$1/resources/installericon.icns ./surgimap.icns

# create ini
echo "[General]" >> ./builds/$1/.installersettings.ini
echo "appName=$2" >> ./builds/$1/.installersettings.ini

# build
echo "Building installer"
cd ./builds/$1

echo "Running qmake"
/usr/local/Qt-5.6.2/bin/qmake ../../SmInstall.pro -r -spec macx-clang

echo "Running make"
make -j4 > report.txt
retcode=$?

if [ $retcode != 0 ]
then
    echo "Problem compiling the installer, showing report.txt..."
    echo " "
    cat report.txt
    exit 1
else
    echo "Compiled installer successfully"
fi

echo "Signing installer"
codesign --deep --verify --sign 228BC59BBB2F284B1AA554F0D751085BEF3DF759 "$2 Installer.app"
retcode=$?

echo "Verifying"
codesign -vvvd "$2 Installer.app"
spctl -a -t exec -vv "$2 Installer.app"

if [ $retcode != 0 ]
then
	echo "codesign returned with non-zero exit code, there may be an issue..."
	exit 1
else
	echo "Signed installer successfully"
fi
