#!/bin/bash

echo "Building Master Package"

buildsuccess=1

cd /Users/surgimap/jenkinsscript

echo "Building branch master"
bash buildbranch.sh master

if [[ $? == 0 ]]; then
    echo "Build master success. Triggering bundle and installer builds."
    bash buildBundle.sh

    if [[ $? == 0 ]]; then
        echo "Core bundle built successfully. Triggering vendor builds."
        buildsuccess=0
    else
        echo "Core bundle build failed. Stopping here.";
    fi;
else
    echo "Build master failed. Stopping here.";
fi

exit $buildsuccess
