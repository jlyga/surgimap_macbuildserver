cd /Users/surgimap/developement/sm_dev/
echo auto build $1
git reset --hard
git pull
git checkout -f $1
git pull


git rev-parse --short $1>out.txt
hashcode=$(head -n 1 out.txt)

lastHashcode=$(head -n 1 lastcommit.txt)
if [ $lastHashcode = $hashcode ] 
then
echo "$1 not changed don't build"
else
echo "$1 changed build for Qa"
echo $hashcode>lastcommit.txt
cd /Users/surgimap/jenkinsscript
bash buildbranch.sh $1
fi