echo lets see if notarize dmg files finished


test -f /Users/surgimap/vendorbuilds/checkDmgFiles.txt
if [[ $? == 0 ]]; then
    echo "we got pending vendore build to see if dmg files are notorized by apple services"
    
else
    echo "nothing to do, see you in the next 5 minute";
    exit -1
fi;

echo check all dmg files here

branch=$(cat /Users/surgimap/vendorbuilds/current_branch.txt)
commit=$(cat /Users/surgimap/vendorbuilds/current_commit.txt)
version=$(cat /Users/surgimap/vendorbuilds/current_version_raw.txt)

xcrun stapler staple -v /MacBuild/networkshare/VendorBuilds/$branch/$version/globusmedical/SM2MacBundleGLOBUSMEDICAL.dmg
if [ $? -eq 0 ]; then
echo "Successfully stepled SM2MacBundleGLOBUSMEDICAL"
else
echo "error SM2MacBundleGLOBUSMEDICAL"
exit -1
fi


xcrun stapler staple -v /MacBuild/networkshare/VendorBuilds/$branch/$version/globusmedical/SurgimapGlobusMedical.dmg
if [ $? -eq 0 ]; then
echo "Successfully stepled SurgimapGlobusMedical"
else
echo "error SurgimapGlobusMedical"
exit -1
fi

xcrun stapler staple -v /MacBuild/networkshare/VendorBuilds/$branch/$version/k2m/SM2MacBundleK2M.dmg
if [ $? -eq 0 ]; then
echo "Successfully stepled SM2MacBundleK2M"
else
echo "error SM2MacBundleK2M"
exit -1
fi

xcrun stapler staple -v /MacBuild/networkshare/VendorBuilds/$branch/$version/k2m/SurgimapK2M.dmg
if [ $? -eq 0 ]; then
echo "Successfully stepled SurgimapK2M"
else
echo "error SurgimapK2M"
exit -1
fi

xcrun stapler staple -v /MacBuild/networkshare/VendorBuilds/$branch/$version/orthofix/SM2MacBundleORTHOFIX.dmg
if [ $? -eq 0 ]; then
echo "Successfully stepled SM2MacBundleORTHOFIX"
else
echo "error SM2MacBundleORTHOFIX"
exit -1
fi

xcrun stapler staple -v /MacBuild/networkshare/VendorBuilds/$branch/$version/orthofix/SurgimapOrthofix.dmg
if [ $? -eq 0 ]; then
echo "Successfully stepled SurgimapOrthofix"
else
echo "error SurgimapOrthofix"
exit -1
fi

xcrun stapler staple -v /MacBuild/networkshare/VendorBuilds/$branch/$version/surgimap/SM2MacBundle.dmg
if [ $? -eq 0 ]; then
echo "Successfully stepled SM2MacBundle"
else
echo "error SM2MacBundle"
exit -1
fi

xcrun stapler staple -v /MacBuild/networkshare/VendorBuilds/$branch/$version/surgimap/Surgimap.dmg
if [ $? -eq 0 ]; then
echo "Successfully stepled Surgimap"
else
echo "error Surgimap"
exit -1
fi

rm /Users/surgimap/vendorbuilds/checkDmgFiles.txt

./mailsend -to "qa@surgimap.com" -from surgimap.auto.compile@gmail.com -starttls -port 587 -auth -smtp smtp.gmail.com -M "All bundles successfully notarized" -sub "Surgimap bundles" -user surgimap.auto.compile -pass Surgimap123


exit 0























