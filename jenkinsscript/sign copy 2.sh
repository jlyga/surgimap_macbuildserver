cd /Users/surgimap/workspace/surgimap/surgimap-2/Surgimap/
echo —use qt deployment tool
echo --add libraries
/Users/surgimap/Qt/5.4/clang_64/bin/macdeployqt "Surgimap for Mac.app"
echo —-add qwt resource file

mkdir "/Users/surgimap/workspace/surgimap/surgimap-2/Surgimap/Surgimap for Mac.app/Contents/Frameworks/qwt.framework/Resources/"

cp /Users/surgimap/workspace/surgimap/surgimap-2/External/build-qwt-Desktop_Qt_5_3_clang_64bit-Release/lib/qwt.framework/Contents/Info.plist "/Users/surgimap/workspace/surgimap/surgimap-2/Surgimap/Surgimap for Mac.app/Contents/Frameworks/qwt.framework/Resources/"

echo ——run the fix python script

python fix.py /Users/surgimap/Qt/5.3/clang_64/ "Surgimap for Mac.app"
 

echo —sign frameworks
codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/Frameworks/QtCore.framework"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/Frameworks/QtConcurrent.framework"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/Frameworks/QtGui.framework"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/Frameworks/QtMultimedia.framework"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/Frameworks/QtMultimediaWidgets.framework"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/Frameworks/QtOpenGL.framework"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/Frameworks/QtPositioning.framework"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/Frameworks/QtPrintSupport.framework"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/Frameworks/QtQml.framework"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/Frameworks/QtQuick.framework"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/Frameworks/QtSensors.framework"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/Frameworks/QtSql.framework"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/Frameworks/QtSvg.framework"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/Frameworks/QtWebKit.framework"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/Frameworks/QtWebKitWidgets.framework"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/Frameworks/QtWidgets.framework"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/Frameworks/QtNetwork.framework"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/Frameworks/qwt.framework"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/Frameworks/libopenjpeg.5.dylib"

echo ---sign plugins


codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/accessible/libqtaccessiblequick.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/accessible/libqtaccessiblewidgets.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/bearer/libqcorewlanbearer.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/bearer/libqgenericbearer.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/mediaservice/libqavfcamera.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/mediaservice/libqqt7engine.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/mediaservice/libqavfmediaplayer.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/mediaservice/libqtmedia_audioengine.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/printsupport/libcocoaprintersupport.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/audio/libqtaudio_coreaudio.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/imageformats/libqdds.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/imageformats/libqgif.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/imageformats/libqicns.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/imageformats/libqico.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/imageformats/libqjp2.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/imageformats/libqjpeg.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/imageformats/libqmng.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/imageformats/libqsvg.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/imageformats/libqsvg_debug.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/imageformats/libqtga.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/imageformats/libqtiff.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/imageformats/libqwbmp.dylib"

codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/imageformats/libqwebp.dylib"


codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/platforms/libqcocoa.dylib"


codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/sqldrivers/libqsqlite.dylib"
codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/sqldrivers/libqsmsqliteee.dylib"
codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/sqldrivers/libqsqlmysql.dylib"
codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/sqldrivers/libqsqlpsql.dylib"
codesign --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app/Contents/PlugIns/sqldrivers/libqsqlodbc.dylib"


echo --sign the app
codesign --deep --force --verify --sign "T375QCFFYE" "Surgimap for Mac.app"

echo --verification
codesign -vvv -d "Surgimap for Mac.app"

spctl -a -t exec -vv "Surgimap for Mac.app"























