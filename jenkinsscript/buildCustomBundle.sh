#!/bin/bash

set -e

# clean up the base bundle files
echo "Cleaning up base bundle files"
cd /Users/surgimap/vendorbuilds/base_bundle
rm application/surgimap.exe || echo "Already removed surgimap.exe from 32-bit folder."
rm application-x64/surgimap.exe || echo "Already removed surgimap.exe from 64-bit folder."
rm -rf Surgimap\ for\ Mac.app || echo "Already removed Surgimap for Mac.app."
rm -rf SurgimapDatabase/qml/* || echo "Already removed qml"

# get latest commit from this branch
echo "Getting latest commit for $1"
if [ $1 = "master" ]
then
export surgimapSrcPath=/Users/surgimap/workspace/surgimap/surgimap-2/Surgimap
else
export surgimapSrcPath=/Users/surgimap/developement/sm_dev/Surgimap
fi

cd $surgimapSrcPath

git pull
git checkout -f $1
git pull

commit=$(git rev-parse --short $1)

# We don't need these since we won't be building installers later
#for field in $(grep "VERSION =" Surgimap/surgimap.pro); do
#version=$field;
#done

#echo version=$version > /Users/surgimap/vendorbuilds/current_version.txt

cd -

# copy the builds into the base bundle
echo "Copying the builds into the base bundle"
diskutil unmount force /MacBuild/copyFolder || echo "/MacBuild/copyFolder alrady unmounted."
diskutil unmount force /MacBuild/networkshare || echo "/MacBuild/networkshare already unmounted."
diskutil unmount force /Volumes/Surgimap || echo "/MacBuild/Surgimap already unmounted."
#mount -t smbfs //automation:1E%23IP%2Aautog1@10.0.10.4/Surgimap /MacBuild/networkshare
#mount -t smbfs //SurgimapFS:Surgimap2%40@10.2.10.110/Surgimap /MacBuild/networkshare

rsync -vrP /MacBuild/networkshare/Builds/Mac/$1/$commit/Surgimap\ for\ Mac.app ./
#cd /MacBuild/networkshare/Builds/win32-static/$1
#rsync -vP $(find $(pwd) | grep $commit | grep exe) /Users/surgimap/vendorbuilds/base_bundle/application/surgimap.exe
cd /MacBuild/networkshare/Builds/win64-static/$1
rsync -vP $(find $(pwd) | grep $commit | grep exe) /Users/surgimap/vendorbuilds/base_bundle/application-x64/surgimap.exe

# copy latest qml
echo "Copying the latest qml into the base bundle"
rsync -vrP /MacBuild/networkshare/Qmlbuilds/qml.latest/* /Users/surgimap/vendorbuilds/base_bundle/SurgimapDatabase/qml/

# clean up permissions
echo "Fixing permissions just in case"
chmod -R 777 /Users/surgimap/vendorbuilds/base_bundle

# Don't need this either
# create new version directory on file share
#echo "Creating new version in builds directory on file share"
#mkdir /MacBuild/networkshare/VendorBuilds/$version || echo "Version folder already created for $version but we will continue anyway."

# create zip and dmg archives
echo "Creating zip and dmg archives"
cd /Users/surgimap/vendorbuilds/shared_assets/SM2WindowsBundle/
rm -rf ./*
rsync -vrP /Users/surgimap/vendorbuilds/base_bundle/* .

rm -rf /Users/surgimap/vendorbuilds/finished_bundles/*
zip -r /Users/surgimap/vendorbuilds/finished_bundles/SM2WindowsBundle.zip ./

# make sure the old template isn't attached or otherwise
hdiutil detach /Volumes/SM2MacBundle || echo "SM2MacBundle already detached"

# attach the template, clean it up and copy the new files over
hdiutil attach -readwrite /Users/surgimap/vendorbuilds/macbundle_template/SM2MacBundleTemplate.dmg
rm -rf /Volumes/SM2MacBundle/Surgimap/*
rsync -vrP /Users/surgimap/vendorbuilds/base_bundle/* /Volumes/SM2MacBundle/Surgimap/

# detach the template and create the new mac bundle with it
hdiutil detach /Volumes/SM2MacBundle
hdiutil convert /Users/surgimap/vendorbuilds/macbundle_template/SM2MacBundleTemplate.dmg -format UDZO -o /Users/surgimap/vendorbuilds/finished_bundles/SM2MacBundle.dmg

# old create command
# hdiutil create -format UDBZ -srcfolder ./ /Users/surgimap/vendorbuilds/finished_bundles/SM2MacWindowsBundle.dmg

# sign the mac bundle
cd /Users/surgimap/vendorbuilds/finished_bundles/
codesign -v -s EB33839DB349397FF1022D2F755FF44C65372DDE SM2MacBundle.dmg
codesign -vvvd SM2MacBundle.dmg
spctl -a -t open -v --context context:primary-signature SM2MacBundle.dmg

# copying to server
echo "Copying finished bundles to File Server"
mkdir -p /MacBuild/networkshare/CustomBuilds/$1/
rsync -vP SM2WindowsBundle.zip /MacBuild/networkshare/CustomBuilds/$1/SM2WindowsBundle.zip
rsync -vP SM2MacBundle.dmg /MacBuild/networkshare/CustomBuilds/$1/SM2MacBundle.dmg

# cleaning up
echo "Cleaning up"
#diskutil unmount force /MacBuild/networkshare || echo "This shouldn't happen but unmounting failed at the end."

echo "Done!"
