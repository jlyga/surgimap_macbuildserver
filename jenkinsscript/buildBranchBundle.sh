#!/bin/bash

set -ex #exit immediately and echo commands

# clean up the base bundle files
echo "Cleaning up base bundle files"
cd /Users/surgimap/vendorbuilds/base_bundle
rm application/surgimap.exe || echo "Already removed surgimap.exe from 32-bit folder."
rm application-x64/surgimap.exe || echo "Already removed surgimap.exe from 64-bit folder."
rm -rf Surgimap\ for\ Mac.app || echo "Already removed Surgimap for Mac.app."
rm -rf SurgimapDatabase/qml/* SurgimapDatabase/www/* SurgimapDatabase/guides/* || echo "Already removed www asset(s)"

# get latest commit from this branch

commit=$(cat /Users/surgimap/vendorbuilds/current_commit.txt)
version=$(cat /Users/surgimap/vendorbuilds/current_version_raw.txt)

echo "shit about to happen here if we call cd -"
#cd -
echo "check current dir"
pwd
# copy the builds into the base bundle
echo "Copying the builds into the base bundle"

#diskutil unmount force /MacBuild/copyFolder || echo "/MacBuild/copyFolder alrady unmounted."
#diskutil unmount force /MacBuild/networkshare || echo "/MacBuild/networkshare already unmounted."
#diskutil unmount force /Volumes/Surgimap || echo "/MacBuild/Surgimap already unmounted."
#mount -t smbfs //automation:1E%23IP%2Aautog1@10.0.10.4/Surgimap /MacBuild/networkshare
#mount -t smbfs //SurgimapFS:Surgimap2%40@10.2.10.110/Surgimap /MacBuild/networkshare

echo "copy surgimap from share drive"


cp -R /MacBuild/networkshare/Builds/Mac/$1/$commit/Surgimap\ for\ Mac.app ./
cd /MacBuild/networkshare/Builds/win32-static/$1
winexe=$(find $(pwd) | grep $commit | grep exe)
if [ -z "$winexe" ]; then
echo "The Windows build server didn't build anything for branch $1 and commit $commit for Win32"
fi


echo "rsync surgimap.exe"
rsync -vP $winexe /Users/surgimap/vendorbuilds/base_bundle/application/surgimap.exe

cd /MacBuild/networkshare/Builds/win64-static/$1
winexe=$(find $(pwd) | grep $commit | grep exe)
if [ -z "$winexe" ]; then
echo "The Windows build server didn't build anything for branch $1 and commit $commit for Win64"
fi
echo "srsync surgimap.exe x64"
rsync -vP $winexe /Users/surgimap/vendorbuilds/base_bundle/application-x64/surgimap.exe

# copy latest www assets
echo "Copying the latest www assets into the base bundle"
cd /Users/surgimap/surgimap_www/
git pull
git checkout -f release
git pull
cp -R qml/* /Users/surgimap/vendorbuilds/base_bundle/SurgimapDatabase/qml/
cp -R guides/* /Users/surgimap/vendorbuilds/base_bundle/SurgimapDatabase/guides/
rm -rf qml/ guides/
cp -R ./* /Users/surgimap/vendorbuilds/base_bundle/SurgimapDatabase/www/
#cp -R /MacBuild/networkshare/Qmlbuilds/qml.latest/* /Users/surgimap/vendorbuilds/base_bundle/SurgimapDatabase/qml/

# remove mac garbage just in case
echo "Removing .DS_Store entries"
find /Users/surgimap/vendorbuilds/base_bundle/ -type f -iname ".DS_Store" -exec rm {} + || echo "Couldn't remove .DS_Store for whatever reason."

# clean up permissions
echo "Fixing permissions just in case"
chmod -R 777 /Users/surgimap/vendorbuilds/base_bundle

# Don't need this either
# create new version directory on file share
#echo "Creating new version in builds directory on file share"
#mkdir /MacBuild/networkshare/VendorBuilds/$version || echo "Version folder already created for $version but we will continue anyway."

# create zip and dmg archives
echo "Creating zip and dmg archives"
cd /Users/surgimap/vendorbuilds/shared_assets/SM2WindowsBundle/
rm -rf ./*
cp -R /Users/surgimap/vendorbuilds/base_bundle/* .

rm -rf /Users/surgimap/vendorbuilds/finished_bundles/*
zip -r -y /Users/surgimap/vendorbuilds/finished_bundles/SM2WindowsBundle.zip ./

# make sure the old template isn't attached or otherwise
hdiutil detach /Volumes/SM2MacBundle || echo "SM2MacBundle already detached"

# attach the template, clean it up and copy the new files over
hdiutil attach -readwrite /Users/surgimap/vendorbuilds/macbundle_template/SM2MacBundleTemplate.dmg
# rm -rf /Volumes/SM2MacBundle/.fseventsd/ || echo ".fseventsd doesn't exist."
rm -rf /Volumes/SM2MacBundle/Surgimap/*
cp -R /Users/surgimap/vendorbuilds/base_bundle/* /Volumes/SM2MacBundle/Surgimap/

# detach the template and create the new mac bundle with it
hdiutil detach /Volumes/SM2MacBundle
hdiutil convert /Users/surgimap/vendorbuilds/macbundle_template/SM2MacBundleTemplate.dmg -format UDZO -o /Users/surgimap/vendorbuilds/finished_bundles/SM2MacBundle.dmg

# old create command
# hdiutil create -format UDBZ -srcfolder ./ /Users/surgimap/vendorbuilds/finished_bundles/SM2MacWindowsBundle.dmg

# sign the mac bundle
cd /Users/surgimap/vendorbuilds/finished_bundles/


# old key
#codesign -v -s EB33839DB349397FF1022D2F755FF44C65372DDE SM2MacBundle.dmg
echo "code sign package with new key expire 2024"
codesign -v -s 228BC59BBB2F284B1AA554F0D751085BEF3DF759 SM2MacBundle.dmg
codesign -vvvd SM2MacBundle.dmg
spctl -a -t open -v --context context:primary-signature SM2MacBundle.dmg

# copying to server
echo "Copying finished bundles to File Server"
mkdir -p /MacBuild/networkshare/VendorBuilds/$1/$version/surgimap/
rsync -vP --inplace SM2WindowsBundle.zip /MacBuild/networkshare/VendorBuilds/$1/$version/surgimap/SM2WindowsBundle.zip
rsync -vP --inplace SM2MacBundle.dmg /MacBuild/networkshare/VendorBuilds/$1/$version/surgimap/SM2MacBundle.dmg

/usr/bin/ditto -c -k --keepParent SM2MacBundle.dmg SM2MacBundle.dmg.zip
xcrun altool --notarize-app --primary-bundle-id $version-$1-$commit --username raulmacule@gmail.com --password ssil-owil-rxgy-utwr --file SM2MacBundle.dmg.zip
rm -f SM2MacBundle.dmg.zip

# cleaning up
echo "Cleaning up"
#umount /MacBuild/networkshare || echo "This shouldn't happen but unmounting failed at the end."

echo "Done!"
