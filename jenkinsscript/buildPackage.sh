echo "Building Mac Installer Package for $2"

# create working dir vars
instsrc="/Users/surgimap/sminstall/builds/$1"
instdest="/Users/surgimap/vendorbuilds/installertmp/mac/$2"

# clean working dir(s)
echo "Cleaning working directory"
rm -rf "$instdest"
mkdir -p "$instdest/.assets"

# copy assets
echo "Copying assets"
cp -vR /Users/surgimap/vendorbuilds/shared_assets/SM2MacWindowsBundle/* "$instdest/.assets/"

# copy ini
cp "$instsrc/.installersettings.ini" "$instdest"

# copy installer
echo "Copying installer"
cp -vR "$instsrc/$2 Installer.app" "$instdest"

# no need to copy installer to notarize
#rm -r "/Users/surgimap/jenkinsscript/binaryToNotarize/$2 Installer.app/"
#cp -vR "$instsrc/$2 Installer.app" "/Users/surgimap/jenkinsscript/binaryToNotarize/"

#if [ $3 = "master" ]
#then
#surgimapAppPath=/Users/surgimap/workspace/surgimap/surgimap-2/Surgimap
#else
#surgimapAppPath=/Users/surgimap/developement/sm_dev/Surgimap
#fi
# no need to notarize since instllers in "/Users/surgimap/sminstall/builds/$1" are noratized once
#bash /Users/surgimap/jenkinsscript/notarize.sh "$2 Installer.app" $surgimapAppPath $3 $4

# create disk image
echo "Creating disk image"
cd "$instdest"
hdiutil create -format UDBZ -srcfolder ./ "$2.dmg"

# sign disk image
echo "Signing disk image and verifying"
codesign -v -s 228BC59BBB2F284B1AA554F0D751085BEF3DF759 "$2.dmg"
codesign -vvvd "$2.dmg"
spctl -a -t open -v --context context:primary-signature "$2.dmg"
